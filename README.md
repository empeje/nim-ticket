# Nim Ticket

The following document describes the application architecture for the upcoming transport booking app for the German & Austrian market. More info about UI preview can be seen [here][COMPLETE_UI].


# Presentation

I have prepared a slide deck for the presentation that can be open in this [Canva link][CANVA_LINK].

# Tech Stacks

## Frontend: Modern React

For the frontend we propose to use Modern React with integrated build tools in Rails. For extra maintainability we will use *TypeScript*.

* Framework: Ruby on Rails with React integration
* Stylesheets: Tailwind CSS with SCSS for ad-hoc styles if needed
* Buildtools: Webpack with compatibility check to keep target browser intact.

Target browsers:
* Chrome * / Chrome mobile (iOS/Android) (*)
* Firefox * / Firefox mobile (iOS/Android) (*)
* Safari / Safari mobile
* IE11 / Edge and above

## Backend: Ruby on Rails

In this project we will use the latest Rails and Ruby versions:

- Rails 7.1
- Ruby 3.2.2

## Tooling

- Repository: GitLab
- CI/CD: GitLab CI
    - Vulnerability check
    - GitLab SAST
    - Code coverage integration

# Architectural details

## API architecture to bind the frontend and backend

```mermaid
sequenceDiagram
    participant User
    participant React
    participant Rails
    participant PostgreSQL
    participant Redis

    User->>React: [User Interaction]
    React->>Rails: API Request
    Rails->>PostgreSQL: Query Database
    PostgreSQL-->>Rails: Query Response
    Rails->>Redis: Check Cache
    Redis-->>Rails: Cache Response
    Rails-->>React: API Response
    React-->>User: Update UI

```

And the following is extended example for booking and payment:

```mermaid
sequenceDiagram
    participant User
    participant Frontend as "React Frontend"
    participant Backend as "Ruby on Rails Backend"
    participant Database as "PostgreSQL Database"
    participant Cache as "Redis Cache"
    participant Payment as "Payment Gateway"
    
    User->>Frontend: Choose Origin, Destination, Date
    Frontend->>Backend: POST /api/search (Search for Transport)
    Backend->>Database: Query for Available Routes
    Database-->>Backend: List of Available Routes
    Backend->>Cache: Check Cache for Availability
    Cache-->>Backend: Cached Data (if available)
    Backend->>Frontend: List of Available Routes
    Frontend->>User: Display Available Routes
    
    User->>Frontend: Select Route and Proceed to Booking
    Frontend->>Backend: POST /api/bookings (Create Booking)
    Backend->>Database: Insert Booking Details
    Database-->>Backend: Confirmation
    Backend->>Cache: Update Cache with Booking Data
    Cache-->>Backend: Cache Updated
    Backend->>Payment: Initiate Payment Process
    Payment-->>Backend: Payment Confirmation
    Backend->>Frontend: Booking Confirmation
    Frontend->>User: Display Booking Confirmation

```

### Potentials API Endpoint

#### User Management:

- `POST /api/users/signup`: Create a new user account.
- `POST /api/users/login`: Authenticate a user and generate an access token.
- `GET /api/users/:id`: Get user details by ID.
- `PUT /api/users/:id`: Update user information.
- `DELETE /api/users/:id`: Delete a user account.
- `POST /api/users/forgot-password`: Initiate a password reset process.
- `POST /api/users/reset-password`: Reset a user's password.

#### Transportation Services:
- `GET /api/services`: Retrieve a list of available transportation services (e.g., airlines, bus companies, train operators).
- `GET /api/services/:id`: Get details of a specific transportation service.

#### Ticket Booking:
- `POST /api/bookings`: Create a new booking for a user.
- `GET /api/bookings/:id`: Get booking details by ID.
- `PUT /api/bookings/:id`: Modify an existing booking.
- `DELETE /api/bookings/:id`: Cancel a booking.

#### Search and Availability:

- `GET /api/search`: Search for available transportation options based on user criteria (e.g., origin, destination, date).
- `GET /api/routes/:id/availability`: Check the availability of seats or tickets for a specific route.

#### Payment and Checkout:

`POST /api/payments`: Initiate a payment for a booking.
`GET /api/payments/:id`: Retrieve payment details by payment ID.
`POST /api/webhooks/payment`: Handle payment-related webhooks/callbacks from payment gateways.

#### Reviews and Ratings:
- `POST /api/reviews`: Submit a review and rating for a transportation service.
- `GET /api/reviews/:id`: Retrieve reviews and ratings for a specific service.

#### User Profile:

- `GET /api/profile`: Retrieve the user's profile information.
- `PUT /api/profile`: Update the user's profile information.

#### Notifications:
- `GET /api/notifications`: Retrieve notifications for the user.
- `PUT /api/notifications/:id`: Mark a notification as read.

#### Search History:
- `GET /api/search-history`: Retrieve a user's search history.
- `DELETE /api/search-history/:id`: Remove a specific search history entry.

#### Ticket History:
- `GET /api/ticket-history`: Retrieve a user's ticket booking history.

#### Geospatial Data:
- `GET /api/locations`: Retrieve a list of available locations for origin and destination selection.

#### Analytics and Reporting:
- `GET /api/analytics/bookings`: Retrieve analytics data for bookings and revenue.
- `GET /api/analytics/users`: Get statistics about user activity.

#### Admin and Management:
- `GET /api/admin/bookings`: View all bookings for administrative purposes.
- `PUT /api/admin/bookings/:id`: Modify a booking (e.g., change seat allocation).
- `DELETE /api/admin/bookings/:id`: Cancel a booking on behalf of a user.
- `POST /api/admin/services`: Add a new transportation service.

## Management of stylesheets and views

This will be straightforward and nothing special except for using Tailwind instead of managing css classes.

## Automated testing strategy (libraries, coverage)

We will make sure the tests are run automatically in CI with the test coverage integration. See the example [here][GITLAB_COVERAGE_TEST].

[COMPLETE_UI]: https://drive.google.com/file/d/1S35U-2YFei2h9YcFEuoFdPq1Y71w9Su-/view
[CANVA_LINK]: https://www.canva.com/design/DAFu_cpnNJc/AXhSY6bFqNG12mc-zfFuaQ/edit?utm_content=DAFu_cpnNJc&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton
[GITLAB_COVERAGE_TEST]: https://www.youtube.com/watch?v=Prrd8ljza-U&t=75s